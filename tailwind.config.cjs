module.exports = {
  darkMode: 'class',
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {
      dropShadow: {
        dark: '12px 12px 0 hsl(221, 39%, 11%)',
        light: '12px 12px 0 hsl(210, 20%, 98%)'
      }
    }
  },
  plugins: []
};

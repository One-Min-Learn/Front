# 1 Min Learn(ing)

Lorem ipsum dolor sit amet

## Project Structure

```
/
├── public/
│   └── favicon.svg
├── src/
│   ├── components/
│   │   ├── containers/
│   │   │   └── index.ts
│   │   │   └── PrimaryContainer.tsx
│   │   │   └── SecondaryContainer.tsx
│   │   └── Button.jsx
│   │   └── Card.jsx
│   │   └── Dots.jsx
│   │   └── Footer.jsx
│   │   └── Header.jsx
│   │   └── SnippetWrapper.jsx
│   ├── layouts/
│   │   └── Layout.astro
│   └── pages/
│       └── [category].astro
│       └── index.astro
│   └── services/
│       └── index.js
│   └── styles/
│       └── dark-syntax.scss
│       └── light-syntax.scss
│       └── tailwind.css
│   └── utils/
│       └── index.ts
└── package.json
```

## Command$

Repository:

```
git clone https://gitlab.com/dmnchzl/one-min-learn.git
```

Install:

```
yarn install
```

Dev:

```
yarn dev
```

Build:

```
yarn build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```

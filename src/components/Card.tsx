import { h } from 'preact';
import clsx from 'clsx';

interface Props {
  className?: string;
  title: string;
  subTitle?: string;
}

export default function Card(props: Props) {
  return (
    <div
      className={clsx(
        'mr-3 mb-3 flex flex-col items-center justify-center bg-gray-50 drop-shadow-dark',
        props.className
      )}>
      <span className="font-monoton text-5xl md:text-6xl text-gray-900">{props.title}</span>
      {props.subTitle && <span className="font-poppins text-gray-700">{props.subTitle}</span>}
    </div>
  );
}

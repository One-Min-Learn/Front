import { h } from 'preact';

/**
 * Visible Island
 */
export default function Footer() {
  const toggleDark = () => document.documentElement.classList.toggle('dark');

  return (
    <footer className="flex flex-grow-0 flex-shrink border-t-4 border-gray-900 dark:border-gray-50">
      <div className="w-16 h-16 flex border-r-4 border-gray-900 dark:border-gray-50">
        <button
          className="m-auto w-8 h-8 bg-gray-900 dark:bg-gray-50 hover:scale-125 transition-transform"
          onClick={toggleDark}
        />
      </div>
      {/* ↑ Toggle Button ↑ */}

      <div className="m-2 flex items-center">
        <span className="font-poppins tracking-wide text-gray-900 dark:text-gray-50">made</span>
        <span className="mx-[2px] font-poppins tracking-wide text-gray-900 dark:text-gray-50">with</span>
        <div className="w-10 h-10 flex bg-rose-300 dark:bg-red-400">
          <span className="m-auto font-jetbrains-mono text-gray-50 dark:text-gray-900">{`<3`}</span>
        </div>
        <span className="mx-[2px] font-poppins tracking-wide text-gray-900 dark:text-gray-50">by</span>
        <a
          href="https://www.dmnchzl.dev"
          target="_blank"
          className="font-poppins font-bold tracking-wide text-gray-900 dark:text-gray-50 hover:text-rose-300 dark:hover:text-red-400 focus:text-rose-300 dark:focus:text-red-400 transition-colors">
          dmnchzl
        </a>
      </div>
    </footer>
  );
}

import { h } from 'preact';
import { useState, useEffect } from 'preact/hooks';

function useObject<T>(initialState: T) {
  const [value, setValue] = useState(initialState);
  return [value, (newValue: Partial<T>) => setValue({ ...value, ...newValue })];
}

interface Snippet {
  title: string;
  content: string;
}

interface Props {
  allSnippets: Snippet[];
}

/**
 * Immediate Island
 */
export default function SnippetWrapper({ allSnippets = [], ...props }: Props) {
  const [snippet, setSnippet] = useObject<Snippet>({
    title: '',
    content: ''
  });

  useEffect(() => {
    const index = Math.floor(Math.random() * allSnippets.length);
    const { title, content } = allSnippets[index];

    setSnippet({ title, content });
  }, []);

  return (
    <>
      <h1 className="font-monoton text-rose-600 dark:text-red-500">{snippet.title}</h1>
      <div dangerouslySetInnerHTML={{ __html: snippet.content }} />
    </>
  );
}

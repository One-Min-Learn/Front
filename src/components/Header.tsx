import { h } from 'preact';
import { useEffect } from 'preact/hooks';

const setThemeColor = (color: string) =>
  document.querySelector('meta[name="theme-color"]').setAttribute('content', color);

/**
 * Lazy Island
 */
export default function Header() {
  useEffect(() => {
    const item = sessionStorage.getItem('theme');

    if (item && item === 'dark') {
      document.documentElement.classList.add('dark');
      setThemeColor('#f87171');
    }
  }, []);

  const toggleTheme = () => {
    const { classList } = document.documentElement;

    if (classList.contains('dark')) {
      document.documentElement.classList.remove('dark');
      sessionStorage.setItem('theme', 'light');
      setThemeColor('#fda4af');
    } else {
      document.documentElement.classList.add('dark');
      sessionStorage.setItem('theme', 'dark');
      setThemeColor('#f87171');
    }
  };

  return (
    <header className="flex flex-grow-0 flex-shrink justify-between border-b-4 border-gray-900 dark:border-gray-50">
      <a href="/" className="m-2 flex items-center">
        <div className="w-10 h-10 flex bg-rose-300 dark:bg-red-400">
          <span className="m-auto font-jetbrains-mono text-gray-50 dark:text-gray-900">01</span>
        </div>
        <span className="mx-[2px] font-poppins font-bold tracking-wide text-gray-900 dark:text-gray-50">min</span>
        <span className="font-poppins tracking-wide text-gray-900 dark:text-gray-50">learning</span>
      </a>

      {/* ↓ Toggle Button ↓ */}
      <div className="w-16 h-16 flex border-l-4 border-gray-900 dark:border-gray-50">
        <button
          className="m-auto w-8 h-8 bg-gray-900 dark:bg-gray-50 hover:scale-125 transition-transform"
          onClick={toggleTheme}
        />
      </div>
    </header>
  );
}

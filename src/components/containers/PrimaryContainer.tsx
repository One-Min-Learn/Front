import { h, ComponentChildren } from 'preact';
import Dots from '../Dots';

interface Props {
  children: ComponentChildren;
}

export default function PrimaryContainer(props: Props) {
  return (
    <div className="relative min-h-[410px] flex flex-col flex-grow">
      <div className="mx-auto p-8 md:p-16 lg:px-24 xl:px-32 w-full z-10">{props.children}</div>

      <div className="absolute px-8 w-full md:h-full flex z-0">
        <Dots className="m-auto" />
      </div>
    </div>
  );
}

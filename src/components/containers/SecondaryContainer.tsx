import { h, ComponentChildren } from 'preact';
import clsx from 'clsx';

interface Props {
  className?: string;
  children: ComponentChildren;
}

export default function SecondaryContainer(props: Props) {
  return (
    <div
      className={clsx(
        'p-8 md:p-16 w-full md:w-[240px] lg:w-[360px] flex flex-col flex-shrink-0 bg-rose-300 dark:bg-red-400 border-t-4 border-l-0 md:border-t-0 md:border-l-4 border-gray-900 dark:border-gray-50',
        props.className
      )}>
      {props.children}
    </div>
  );
}

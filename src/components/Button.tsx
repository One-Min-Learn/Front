import { h, ComponentChildren } from 'preact';
import clsx from 'clsx';

interface Props {
  href?: string;
  className?: string;
  children: ComponentChildren;
}

export default function Button({ href = '/', ...props }: Props) {
  return (
    <a href={href} className={clsx('retro-button', props.className)}>
      <span className="font-jetbrains-mono">{props.children}</span>
    </a>
  );
}

import { unified } from 'unified';
import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import rehypeSanitize, { defaultSchema } from 'rehype-sanitize';
import rehypePrism from 'rehype-prism';
import rehypeStringify from 'rehype-stringify';
import { visit, Node } from 'unist-util-visit';
import { H_CLASS_NAME, P_CLASS_NAME, A_CLASS_NAME, PRE_CLASS_NAME } from '../constants';

export function sortBy<T>(key: string) {
  return (a: T, b: T) => (a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0);
}

const mergeClassName = (node: Node, className = ''): string => {
  const currentClassName = node.properties.className || [];
  return [...currentClassName, ...className.split(/\s/)]; // RegExp 'Space'
};

function rehypeClass() {
  return (tree, file) => {
    visit(tree, (node: Node) => {
      if (node.type === 'element') {
        // RegExp 'startsWith()'
        if (/^h/.test(node.tagName)) {
          node.properties.className = mergeClassName(node, H_CLASS_NAME);
        }

        if (node.tagName === 'p' && node.value !== '\n') {
          node.properties.className = mergeClassName(node, P_CLASS_NAME);
        }

        if (node.tagName === 'a') {
          node.properties.className = mergeClassName(node, A_CLASS_NAME);
        }

        // const [child] = node.children;
        // if (child?.type === "element" && child?.tagName === "code")

        if (node.tagName === 'pre') {
          node.properties.className = mergeClassName(node, PRE_CLASS_NAME);
        }
      }
    });
  };
}

export const markdownToHtml = async (markdown: string): string => {
  const result = await unified()
    .use(remarkParse)
    .use(remarkRehype)
    .use(rehypeSanitize, {
      ...defaultSchema,
      attributes: {
        ...defaultSchema.attributes,
        pre: [...(defaultSchema.attributes.pre || []), ['className']],
        code: [...(defaultSchema.attributes.code || []), ['className']]
      }
    })
    .use(rehypePrism)
    .use(rehypeClass)
    .use(rehypeStringify)
    .process(markdown);

  return result.toString();
};

import MockCategories from './__mocks__/Categories.json';
import MockQuotes from './__mocks__/Quotes.json';
import MockSnippets from './__mocks__/Snippets.json';

const CMS_API = import.meta.env['CMS_API'];

interface Category {
  title: string;
  subTitle: string;
}

interface Quote {
  author: string;
  content: string;
}

interface Snippet {
  title: string;
  content: string;
  categories: {
    title: string;
  }[];
}

interface Response<T> {
  docs: T[];
  totalDocs: number;
}

export function getAllCategories(): Promise<Response<Category>> {
  return fetch(`${CMS_API}/categories`)
    .then(response => {
      if (response.statusText === 'OK') {
        return response.json();
      }

      throw new Error('Something Went Wrong...');
    })
    .catch(() => ({
      docs: MockCategories,
      totalDocs: 1
    }));
}

export function getAllQuotes(): Promise<Response<Quote>> {
  return fetch(`${CMS_API}/quotes`)
    .then(response => {
      if (response.statusText === 'OK') {
        return response.json();
      }

      throw new Error('Something Went Wrong...');
    })
    .catch(() => ({
      docs: MockQuotes,
      totalDocs: 1
    }));
}

export function getAllSnippets(): Promise<Response<Snippet>> {
  return fetch(`${CMS_API}/snippets`)
    .then(response => {
      if (response.statusText === 'OK') {
        return response.json();
      }

      throw new Error('Something Went Wrong...');
    })
    .catch(() => ({
      docs: MockSnippets,
      totalDocs: 1
    }));
}

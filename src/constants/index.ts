export const H_CLASS_NAME = 'font-monoton text-rose-600 dark:text-red-500';
export const P_CLASS_NAME = 'my-4 font-poppins text-gray-700 dark:text-gray-200';
// prettier-ignore
export const A_CLASS_NAME = "hover:text-rose-300 dark:hover:text-red-400 focus:text-rose-300 dark:focus:text-red-400 transition-colors";
// prettier-ignore
export const PRE_CLASS_NAME = "mt-3 mr-6 mb-6 ml-3 p-4 text-gray-900 bg-gray-50 dark:bg-gray-900 dark:text-gray-50 border-4 border-gray-900 dark:border-gray-50 drop-shadow-dark dark:drop-shadow-light rounded-none overflow-x-auto";
